# README #

### Steps to setup this application: ###

* cmd: composer install
* Edit Database name in env file (DB_DATABASE='database_name')
* cmd: php artisan migrate
* cmd: php artisan passport:install --uuids
* Create Robusta's passport client which will grant tokens to our app/web users.
	* php artisan passport:client --password
* Copy passport client id and passport client secret and store safely to use later to generate token

### Before tesitng make sure you: ###

* created a user
* generated a token for that user
* Inserted bearer token with requests otherwise middleware won't allow the request

### To generate user token ###

* Send post request to APP_URL/oauth/token.
* Body:
	* grant_type:password
	* client_id:passport_client_id generated earlier
	* client_secret:passport client secret
	* username:user_email
	* password:user_password
	* scope:''
* Refer to https://laravel.com/docs/8.x/passport#password-grant-tokens for more info
