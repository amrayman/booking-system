-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2021 at 11:55 PM
-- Server version: 5.7.17
-- PHP Version: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bus-booking`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `uuid` binary(16) NOT NULL,
  `seat_id` binary(16) NOT NULL,
  `stop_id` binary(16) NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`uuid`, `seat_id`, `stop_id`, `status`, `created_at`, `updated_at`) VALUES
(0x86c2eea699e1482e8ee4e7c61e914b2a, 0xddac3ba149dc42d1b136a70efc4cf3ee, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0x4f15b5c06dc44682a7ca55c2ddb2895b, 0x607bf2f6c93e47d28a4cb1a546428724, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0x6732b653e08741b78aa1d28fefafcbfd, 0x0d4c1b3ec9ce42eb8733628eb134a9c5, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0x46784c096702422c932332e3dfa9048f, 0xfebc37fd33404b8398d3f1b6deb5942b, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0x00003ef7d6604588b9c5270e4875b05e, 0xe18c40db9e69434faa45628e62bdf53d, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0xe5e149fc3ff342d1a2d3a6924383b223, 0xd95e8f8095ea40fbbc8b9565d7148513, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0x5eef060e97374ea79c4b9b361bdad700, 0x8fe8a74774bf43769ed1f49296a4df02, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0xc540ecbad04f470c813b6c6a4fdc7df5, 0xee0d4440742046179d11d875d7b8f6d3, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0xbbe9d747c7b8419ea0d71c5d5bb9f7a8, 0x78dff5f5fc53422992e6900925447e63, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0x7e75c17843d743f19ba3670e59ef8005, 0x436f7d4f96c945b7b5878bfd7a1b8548, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0x00357eb816fc489b86455bf98e35267c, 0xa94621ae12f24711acd67d385910f3ca, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0x187da7120f054053a7f2792dab5dad99, 0x0c78ab3d4cf34bffa6ca01396d4b3034, 0xe6311f8428df4b28821d77ebf3e403a5, 0, NULL, NULL),
(0xc9c7633d56464af6a15089f67e8a070f, 0xddac3ba149dc42d1b136a70efc4cf3ee, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0x7673e5d84c8b4276b33cea5a29bd32ef, 0x607bf2f6c93e47d28a4cb1a546428724, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0x41b1b7ef1ef14ee4b2c6c803bf161b79, 0x0d4c1b3ec9ce42eb8733628eb134a9c5, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0xc9ce533747d542d2ab9be502561267f4, 0xfebc37fd33404b8398d3f1b6deb5942b, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0x3b1d3fa2b67c420dae118613c45b0076, 0xe18c40db9e69434faa45628e62bdf53d, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0x8ee5aae5c9b0471594d5a72b2e72b386, 0xd95e8f8095ea40fbbc8b9565d7148513, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0x70cf724d45ed42d3b33f3df102610d81, 0x8fe8a74774bf43769ed1f49296a4df02, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0xc41b03a45b6e45e4925c416ae6476a32, 0xee0d4440742046179d11d875d7b8f6d3, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0x04eccf25e0724677918f3b138ba2d89e, 0x78dff5f5fc53422992e6900925447e63, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0xf1b60cb1134d48fd97c9a413911cabc4, 0x436f7d4f96c945b7b5878bfd7a1b8548, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0xf14002e136434000ac1031fe2c7f7b90, 0xa94621ae12f24711acd67d385910f3ca, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0x5c1a50285e5c4921926569a318285f88, 0x0c78ab3d4cf34bffa6ca01396d4b3034, 0xe5e24952975a4911abbf43f863511d46, 0, NULL, NULL),
(0x9e88c10b25f8429fb56d7e13a7770d50, 0xddac3ba149dc42d1b136a70efc4cf3ee, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL),
(0x10e4c7e5b75b498f805ca88e8ac65263, 0x607bf2f6c93e47d28a4cb1a546428724, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL),
(0x65138bcedc2b4c35a269a440c2d246cf, 0x0d4c1b3ec9ce42eb8733628eb134a9c5, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL),
(0x45f33a381e694936a856574c76bd9826, 0xfebc37fd33404b8398d3f1b6deb5942b, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL),
(0x45b7823991224f9d87ef6dc4c874e4ce, 0xe18c40db9e69434faa45628e62bdf53d, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL),
(0x05b5205afdde46e39117126d358d73e8, 0xd95e8f8095ea40fbbc8b9565d7148513, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL),
(0x726a8787ebf8455888bc55430e9a900b, 0x8fe8a74774bf43769ed1f49296a4df02, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL),
(0x2e55b407669f497597ce593fad70d6f6, 0xee0d4440742046179d11d875d7b8f6d3, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL),
(0x4f4957abc7664c1dbe1150eb79861ebd, 0x78dff5f5fc53422992e6900925447e63, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL),
(0x220c98077d374a5ebe7e24bebe4f0482, 0x436f7d4f96c945b7b5878bfd7a1b8548, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL),
(0x1ba8f658648b4fa395116eae34735350, 0xa94621ae12f24711acd67d385910f3ca, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL),
(0xb8006f70a0a94ec7b61e9a10f54beba8, 0x0c78ab3d4cf34bffa6ca01396d4b3034, 0x9e0b1be786bb440aa2f38a0976ebd054, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `buses`
--

CREATE TABLE `buses` (
  `uuid` binary(16) NOT NULL,
  `verbose` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buses`
--

INSERT INTO `buses` (`uuid`, `verbose`) VALUES
(0x0a56b70abb2946bf9c49c156d5d5ba61, 'Mercedes'),
(0x059f2190a9414105bbb3bb3bad974ed9, 'Chevorlet'),
(0x063226e1cc3e41bf97cdc59a0e589c29, 'Aston Martin'),
(0x31033d196ece40bb83fd5433a53c43fe, 'Ferrari');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2014_10_12_000000_create_users_table', 1),
(32, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(33, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(34, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(35, '2016_06_01_000004_create_oauth_clients_table', 1),
(36, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(37, '2021_03_19_194918_create_stations_table', 1),
(38, '2021_03_19_203804_create_buses_table', 1),
(43, '2021_03_19_214501_create_seats_table', 2),
(44, '2021_03_20_101242_create_trips_table', 2),
(45, '2021_03_20_102830_create_stops_table', 2),
(46, '2021_03_20_102932_create_bookings_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('74998c4b12c3bd1fd3aba5cc0fed20b1f0982ae672159d5bf22b37d1eaedac4f73724e0650026996', 1, '92ff451f-32b7-4f76-918b-d1cfca8cc7da', NULL, '[]', 0, '2021-03-20 20:01:35', '2021-03-20 20:01:35', '2021-03-27 22:01:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
('92ff44a4-52f3-463f-ab42-84f7e3266fd7', NULL, 'RobustaOwn', '$2y$10$CfMFRSMowWOtvh.uhklNuOA1TpWckUmlBRqesHKBjHEelAFS9je0C', NULL, '', 0, 0, 0, '2021-03-20 12:30:59', '2021-03-20 12:30:59'),
('92ff451f-32b7-4f76-918b-d1cfca8cc7da', NULL, 'Robusta', '$2y$10$eoxxD6/9iTmYI58siIqpbubdms1ISGrM.eobPbpZikNcX7d9CTxte', 'users', 'http://localhost', 0, 1, 0, '2021-03-20 12:32:19', '2021-03-20 12:32:19');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('4bbc72c3e8c91cbd681553e947f2e80794a10276a7ddc632c535a62329e1229a27d52e284f63828e', '74998c4b12c3bd1fd3aba5cc0fed20b1f0982ae672159d5bf22b37d1eaedac4f73724e0650026996', 0, '2022-03-20 22:01:35');

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE `seats` (
  `uuid` binary(16) NOT NULL,
  `bus_id` binary(16) NOT NULL,
  `seat_num` tinyint(3) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seats`
--

INSERT INTO `seats` (`uuid`, `bus_id`, `seat_num`) VALUES
(0x3393b794e9234519bc0cabae408a7435, 0x31033d196ece40bb83fd5433a53c43fe, 4),
(0x706d33e0628e45b4ac366cddad7a4339, 0x31033d196ece40bb83fd5433a53c43fe, 3),
(0x5f44d64aa08d44099674b5ca056bae8b, 0x31033d196ece40bb83fd5433a53c43fe, 2),
(0x1d6505c3e4144cf89f434fb7e953dfb1, 0x31033d196ece40bb83fd5433a53c43fe, 1),
(0xdb64dd32fd634a2e8896d03e374bab61, 0x31033d196ece40bb83fd5433a53c43fe, 5),
(0x86da0939043142b098011e515faae7ea, 0x31033d196ece40bb83fd5433a53c43fe, 6),
(0xba1f8f9b7ad345e6bf9a71638b62b53c, 0x31033d196ece40bb83fd5433a53c43fe, 7),
(0x14f4f7b84bb34380b170b1ee11a93efb, 0x31033d196ece40bb83fd5433a53c43fe, 8),
(0x86e39ee9859040898e8b071718a456bd, 0x31033d196ece40bb83fd5433a53c43fe, 9),
(0x81e890da6488446a81d69aa00a7ab58f, 0x31033d196ece40bb83fd5433a53c43fe, 10),
(0x5363ff737a8a471f81d0a737578c062a, 0x31033d196ece40bb83fd5433a53c43fe, 11),
(0x3bc806bba53c414c9ee011c97884c6a4, 0x31033d196ece40bb83fd5433a53c43fe, 12),
(0x0c78ab3d4cf34bffa6ca01396d4b3034, 0x063226e1cc3e41bf97cdc59a0e589c29, 1),
(0xa94621ae12f24711acd67d385910f3ca, 0x063226e1cc3e41bf97cdc59a0e589c29, 2),
(0x436f7d4f96c945b7b5878bfd7a1b8548, 0x063226e1cc3e41bf97cdc59a0e589c29, 3),
(0x78dff5f5fc53422992e6900925447e63, 0x063226e1cc3e41bf97cdc59a0e589c29, 4),
(0xee0d4440742046179d11d875d7b8f6d3, 0x063226e1cc3e41bf97cdc59a0e589c29, 5),
(0x8fe8a74774bf43769ed1f49296a4df02, 0x063226e1cc3e41bf97cdc59a0e589c29, 6),
(0xd95e8f8095ea40fbbc8b9565d7148513, 0x063226e1cc3e41bf97cdc59a0e589c29, 7),
(0xe18c40db9e69434faa45628e62bdf53d, 0x063226e1cc3e41bf97cdc59a0e589c29, 8),
(0xfebc37fd33404b8398d3f1b6deb5942b, 0x063226e1cc3e41bf97cdc59a0e589c29, 9),
(0x0d4c1b3ec9ce42eb8733628eb134a9c5, 0x063226e1cc3e41bf97cdc59a0e589c29, 10),
(0x607bf2f6c93e47d28a4cb1a546428724, 0x063226e1cc3e41bf97cdc59a0e589c29, 11),
(0xddac3ba149dc42d1b136a70efc4cf3ee, 0x063226e1cc3e41bf97cdc59a0e589c29, 12);

-- --------------------------------------------------------

--
-- Table structure for table `stations`
--

CREATE TABLE `stations` (
  `uuid` binary(16) NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `long` decimal(9,6) DEFAULT NULL,
  `lat` decimal(9,6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stations`
--

INSERT INTO `stations` (`uuid`, `city`, `long`, `lat`) VALUES
(0xa47c452c71ab49dc958f3b86baad6743, 'Cairo', NULL, NULL),
(0x51cddf8615ce4b4a9687e1fadb47738b, 'Luxor', NULL, NULL),
(0xcf8beb5f19764877a01af5ec0ab85f76, 'Alexandria', NULL, NULL),
(0x640a17405d494c8ba01c31e56d3b274a, 'Aswan', NULL, NULL),
(0x9dfb90bf528347539d9a39d6aa373b9d, 'Fayoum', NULL, NULL),
(0xb076ff6cd8d6484d8b3cd33052ed7fbb, 'Asyut', NULL, NULL),
(0x34134643eb304d2a84e82b6d04495c97, 'Giza', NULL, NULL),
(0xc2de5e89046b46c58c664bb6009cf082, 'Suez', NULL, NULL),
(0x1cc59727fd944513bc3fd66414dacfb9, 'Sohag', NULL, NULL),
(0x6ab1f32b2eb34216a8af478a061daadb, 'Minya', NULL, NULL),
(0x95c59ee1e228421aa159e914365b94c9, 'Matruh', NULL, NULL),
(0x32011c7f54e24effba1c7f37fae729a2, 'Ismailia', NULL, NULL),
(0x4570b8b7a46547bbbc86ffae50b2b831, 'Port-Said', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stops`
--

CREATE TABLE `stops` (
  `uuid` binary(16) NOT NULL,
  `station_id` binary(16) NOT NULL,
  `trip_id` binary(16) NOT NULL,
  `order` smallint(5) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stops`
--

INSERT INTO `stops` (`uuid`, `station_id`, `trip_id`, `order`) VALUES
(0xe6311f8428df4b28821d77ebf3e403a5, 0x4570b8b7a46547bbbc86ffae50b2b831, 0xaeb2080080854993b2bb5ef6b47f5ab6, 3),
(0xe5e24952975a4911abbf43f863511d46, 0x32011c7f54e24effba1c7f37fae729a2, 0xaeb2080080854993b2bb5ef6b47f5ab6, 2),
(0x9e0b1be786bb440aa2f38a0976ebd054, 0x34134643eb304d2a84e82b6d04495c97, 0xaeb2080080854993b2bb5ef6b47f5ab6, 1),
(0xbdba01c5d2f0450eb0b632404028f1ea, 0xa47c452c71ab49dc958f3b86baad6743, 0xaeb2080080854993b2bb5ef6b47f5ab6, 0),
(0x78f7e671dd39423aba05d45e295c4faa, 0xa47c452c71ab49dc958f3b86baad6743, 0xab01670fc1394df9ba61255bd6d61306, 0),
(0x9672c455b17648a0a71c19a14e7d1c7d, 0x34134643eb304d2a84e82b6d04495c97, 0xab01670fc1394df9ba61255bd6d61306, 1),
(0x62d10560fbb44adc8dd677a1f9f88fa9, 0x9dfb90bf528347539d9a39d6aa373b9d, 0xab01670fc1394df9ba61255bd6d61306, 2),
(0xc3fee17af9024d67a7243f950ced8fc5, 0xb076ff6cd8d6484d8b3cd33052ed7fbb, 0xab01670fc1394df9ba61255bd6d61306, 3),
(0x3bb79911d42e4c24a7fd52e7da4e8ff6, 0x51cddf8615ce4b4a9687e1fadb47738b, 0xab01670fc1394df9ba61255bd6d61306, 4),
(0x0e3ac3e440dd44759cdac0e1ba3b3b86, 0x640a17405d494c8ba01c31e56d3b274a, 0xab01670fc1394df9ba61255bd6d61306, 5);

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `uuid` binary(16) NOT NULL,
  `bus_id` binary(16) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verbose` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`uuid`, `bus_id`, `name`, `verbose`) VALUES
(0xab01670fc1394df9ba61255bd6d61306, 0x0a56b70abb2946bf9c49c156d5d5ba61, 'CAI2ASW', NULL),
(0xaeb2080080854993b2bb5ef6b47f5ab6, 0x063226e1cc3e41bf97cdc59a0e589c29, 'CAI2PORT', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` binary(16) NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `first_name`, `last_name`, `email`, `password`, `phone_number`, `created_at`, `updated_at`) VALUES
(1, 0xd165f09c2a824d158398eaab18146716, 'Amro', 'Ghee', 'email@email.com', '$2y$10$/xhxAuu0VtrYQScSi6q4KOpT4PjdIMaEGDlchmtNc1Nf0AoaJWvma', '01210010086', '2021-03-20 20:00:17', '2021-03-20 20:00:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD UNIQUE KEY `bookings_uuid_unique` (`uuid`);

--
-- Indexes for table `buses`
--
ALTER TABLE `buses`
  ADD UNIQUE KEY `buses_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `seats`
--
ALTER TABLE `seats`
  ADD UNIQUE KEY `seats_uuid_unique` (`uuid`);

--
-- Indexes for table `stations`
--
ALTER TABLE `stations`
  ADD UNIQUE KEY `stations_uuid_unique` (`uuid`);

--
-- Indexes for table `stops`
--
ALTER TABLE `stops`
  ADD UNIQUE KEY `stops_uuid_unique` (`uuid`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD UNIQUE KEY `trips_uuid_unique` (`uuid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_uuid_unique` (`uuid`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_number_unique` (`phone_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
