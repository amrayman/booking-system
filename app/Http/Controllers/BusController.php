<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Bus as Bus;
use App\Models\Seat as Seat;

class BusController extends Controller
{
    //
	public function create(Request $request)
	{
		$bus = new Bus;
		$bus->verbose = $request->input("verbose");
		$bus->save();
		
		for($i = 1; $i <= 12; $i++)
		{
			$seat = new Seat;
			$seat->bus_id = $bus->uuid;
			$seat->seat_num = $i;
			$seat->save();
		}
		
		return $bus;
	}
}
