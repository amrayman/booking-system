<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Models\Trip as Trip;
use App\Models\Stop as Stop;
use App\Models\Booking as Booking;

class BookingController extends Controller
{
    //
	
	/**
	* List all available seats for trips from origin to destination
	*
	* @bodyParam payload object required 
	* @bodyParam payload.origin string required uuid of the origin station.
	* @bodyParam payload.destination string required uuid of the destination station.
	*
	*		
	* @response status=422 scenario="input parameters validation failed"
	{
		"success" : False,
		"code" : 422,
		"message" : "Validation Failure",
		"data" : [{ "errors" : [Validator Errors] }]
	}
	*
	* @response
	{
	"success" : False,
	"code" : 404,
	"errors" : "Trip doesn't exist";
	}
	*
	* @response {
		"success" : True,
		"code" : 200,
		"data" : [seat_id]
	}
	*
	*/
	public function index(Request $request)
	{
		
		$rules=[
		"origin"=> "required|uuid",
		"destination"=> "required|uuid",
		];

		$messages = [
		"required" => "The :attribute is missing",
		"uuid" => "The :attribute must only be a valid uuid",
		];
            
		$validator = Validator::make($request->input("payload"), $rules, $messages);
		if ($validator->fails())
		{
			//pass validator errors as errors object for response
			$return["success"]=False;
			$return["code"]=422;
			$return_object["message"]="Validation Failure";
			$return["errors"]=$validator->errors()->all();
			return $return;
		}
		$trips = Trip::passingBy($request->input("payload.origin"),$request->input("payload.destination"))->get();	
		
		if($trips->isEmpty())
		{
			$return["success"]=False;
			$return["code"]=404;
			$return["errors"]="Trip doesn't exist";
			return $return;
		}
		else
		{
			$result = []; 
			foreach ($trips as $trip)
			{
				$temp_result = [];
				$visitng_order1 = Stop::StationtripFilter($request->input("payload.origin"), $trip->uuid)->first()->order;
				$visitng_order2 = Stop::StationtripFilter($request->input("payload.destination"), $trip->uuid)->first()->order;
				if ($visitng_order1 > $visitng_order2)
				{
					continue;
				}
				else 
				{
					$stops = Stop::TripFilter($trip->uuid)->get();
					foreach ($stops as $stop)
					{
						if ($stop->order < $visitng_order1+1)
							continue;			
						if ($stop->order > $visitng_order2)
							break;
							
						$bookings = Booking::StopVacantFilter($stop->uuid)->get();
						foreach ($bookings as $booking)
						{
							if (array_key_exists($booking->seat_id, $temp_result))
								$temp_result[$booking->seat_id]+=1;
							else
								$temp_result[$booking->seat_id]=1;
						}
					}
				}
				foreach ($temp_result as $key => $value)
				{
					if ($value == ($visitng_order2-$visitng_order1))
					{
						array_push($result, $key);
					}
				}
			}
			$return["success"]=True;
			$return["code"]=200;
			$return["data"]=$result;
			return $return;
		}
	}
	
}
