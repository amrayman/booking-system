<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\Trip as Trip;
use App\Models\Stop as Stop;
use App\Models\Seat as Seat;
use App\Models\Booking as Booking;

class TripController extends Controller
{
    //
	
	public function create(Request $request)
	{
		$trip = new Trip;
		$trip->bus_id = $request->input("bus_id");
		$trip->name = $request->input("name");
		$trip->save();
		
		$cities = $request->input("cities");
		$visiting_order = 0;
		$origin = True;
		
		$seats = Seat::InBus($request->input("bus_id"))->get();
		
		foreach($cities as $city)
		{
			$stop = new Stop;
			$stop->station_id = $city;
			$stop->trip_id = $trip->uuid;
			$stop->order = $visiting_order;
			$stop->save();
			$visiting_order+=1;
			
			if ($origin)
			{
				$origin = False;
				continue;
			}
			
			foreach($seats as $seat)
			{
				$booking = new Booking;
				$booking->seat_id = $seat->uuid;
				$booking->stop_id = $stop->uuid;
				$booking->save();
			}
			
		}
	}
	
	public function showRoute(Request $request)
	{
		$stops = Stop::TripFilter($request->route("id"))->get();
		return $stops;
	}
}
