<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;

use App\Models\User as User;

class UserController extends Controller
{
    //
	public function create(Request $request)
	{
		$entity = new User;
		$entity->first_name = "Amro";
		$entity->last_name = "Ghee";
		$entity->phone_number = "01210010086";
		$entity->email = "email@email.com";
		$entity->password = Hash::make("2672");
		
		$entity->save();
		
		return $entity;
	}
		public function show(Request $request)
	{
		
		return auth('api')->user();
	}
	
}
