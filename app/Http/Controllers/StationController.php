<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Station as Station;

class StationController extends Controller
{
    //
	public function create(Request $request)
	{
		$entity = new Station;
		$entity->city = $request->input("city");
		$entity->save();
		return $entity;
	}
}
