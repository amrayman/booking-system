<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Dyrynda\Database\Casts\EfficientUuid;
use Dyrynda\Database\Support\GeneratesUuid;

class Stop extends Model
{
    use HasFactory, GeneratesUuid;
	
	public $timestamps = false;
	public $incrementing = false;
	/**
	* The table associated with the model.
	*
	* @var string
	*/
	protected $table = "stops";
	
	/**
	* The database connection that should be used by the model.
	*
	* @var string
	*/
	protected $connection = "mysql";
	
	/**
	* The primary key associated with the table.
	*
	* @var string
	*/
	protected $primaryKey = "uuid";
	
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
	
	/**
	 * The model default values for attributes.
	 *
	 * @var array
	 */
	protected $attributes = [
	];
	/**
	* The attributes that should be cast to native types.
	*
	* @var array
	*/
	protected $casts = [
		'uuid' => EfficientUuid::class,
		'station_id' => EfficientUuid::class,
		'trip_id' => EfficientUuid::class,
	];
	
	public function uuidColumns(): array
    {
        return ['uuid', 'station_id', 'trip_id'];
    }
	 /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
	
	public function scopeTripFilter($query, $trip_id)
    {
        return $query->WhereUuid($trip_id, 'trip_id')->orderBy('order');
    }
	
	public function scopeStationFilter($query, $station_id)
    {
        return $query->WhereUuid($station_id, 'station_id');
    }
	
	public function scopeStationTripFilter($query, $station_id, $trip_id)
    {
        return $query->WhereUuid($station_id, 'station_id')->WhereUuid($trip_id, 'trip_id');
    }
}
