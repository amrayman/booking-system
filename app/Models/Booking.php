<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Dyrynda\Database\Casts\EfficientUuid;
use Dyrynda\Database\Support\GeneratesUuid;

class Booking extends Model
{
    use HasFactory, GeneratesUuid;
	
	public $timestamps = false;
	public $incrementing = false;
	/**
	* The table associated with the model.
	*
	* @var string
	*/
	protected $table = "bookings";
	
	/**
	* The database connection that should be used by the model.
	*
	* @var string
	*/
	protected $connection = "mysql";
	
	/**
	* The primary key associated with the table.
	*
	* @var string
	*/
	protected $primaryKey = "uuid";
	
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
	
	/**
	 * The model default values for attributes.
	 *
	 * @var array
	 */
	protected $attributes = [
		'status' => 0,
	];
	
	 /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
	
	 /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'uuid' => EfficientUuid::class,
		'seat_id' => EfficientUuid::class,
		'stop_id' => EfficientUuid::class,
    ];
	public function uuidColumns(): array
    {
        return ['uuid', 'seat_id', 'stop_id'];
    }
	
    public function seat()
    {
        return $this->belongsTo(Seat::class, 'seat_id');
    }
	
	public function stop()
    {
        return $this->belongsTo(Stop::class, 'stop_id');
    }
	
	public function scopeStopVacantFilter($query, $stop_id)
    {
        return $query->whereUuid($stop_id, 'stop_id')->where('status', 0);
    }
}
