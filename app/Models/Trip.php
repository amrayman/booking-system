<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Dyrynda\Database\Casts\EfficientUuid;
use Dyrynda\Database\Support\GeneratesUuid;

class Trip extends Model
{
    use HasFactory, GeneratesUuid;
	
	public $timestamps = false;
	public $incrementing = false;
	/**
	* The table associated with the model.
	*
	* @var string
	*/
	protected $table = "trips";
	
	/**
	* The database connection that should be used by the model.
	*
	* @var string
	*/
	protected $connection = "mysql";
	
	/**
	* The primary key associated with the table.
	*
	* @var string
	*/
	protected $primaryKey = "uuid";
	
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
	
	/**
	 * The model default values for attributes.
	 *
	 * @var array
	 */
	protected $attributes = [
		"verbose" => Null,
	];
	
	 /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
	
	/**
	* The attributes that should be cast to native types.
	*
	* @var array
	*/
	protected $casts = [
		'uuid' => EfficientUuid::class,
		'bus_id' => EfficientUuid::class,
	];
	
	public function uuidColumns(): array
    {
        return ['uuid', 'bus_id',];
    }
	public function stops()
    {
        return $this->hasMany(Stop::class, "trip_id", "uuid");
    }
	
	public function scopePassingBy(Builder $query, $origin, $destination)
    {
        return $query->whereHas('stops', function (Builder $q1) use ($origin)
							{ $q1->whereUuid($origin, 'station_id'); })
					 ->whereHas('stops', function (Builder $query) use ($destination)
							{ $query->whereUuid($destination, 'station_id'); });
    }
}
