<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Laravel\Passport\HasApiTokens;

use Dyrynda\Database\Casts\EfficientUuid;
use Dyrynda\Database\Support\GeneratesUuid;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, GeneratesUuid;
	
	/**
	* The table associated with the model.
	*
	* @var string
	*/
	protected $table = "users";

	/**
	* The database connection that should be used by the model.
	*
	* @var string
	*/
	protected $connection = "mysql";
	
	/**
	* The primary key associated with the table.
	*
	* @var string
	*/
	protected $primaryKey = "id";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
		'id',
        'password',
        #'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        #'email_verified_at' => 'datetime',
		'uuid' => EfficientUuid::class,
    ];
}
