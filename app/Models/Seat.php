<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Dyrynda\Database\Casts\EfficientUuid;
use Dyrynda\Database\Support\GeneratesUuid;

class Seat extends Model
{
    use HasFactory, GeneratesUuid;
	
	public $timestamps = false;
	public $incrementing = false;
		/**
	* The table associated with the model.
	*
	* @var string
	*/
	protected $table = "seats";
	
	/**
	* The database connection that should be used by the model.
	*
	* @var string
	*/
	protected $connection = "mysql";
	
	/**
	* The primary key associated with the table.
	*
	* @var string
	*/
	protected $primaryKey = "uuid";
	
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
	 /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'uuid' => EfficientUuid::class,
		'bus_id' => EfficientUuid::class,
    ];
    public function uuidColumns(): array
    {
        return ['uuid', 'bus_id'];
    }
	
	/**
	 * The model default values for attributes.
	 *
	 * @var array
	 */
	protected $attributes = [
	];
	
	 /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
	
	public function scopeInBus($query, $bus_id)
    {
        return $query->WhereUuid($bus_id, 'bus_id');
    }
	
	#public function stops()
    #{
    #    return $this->belongsToMany(Stop::class, 'bookings', 'seat_id', 'stop_id');
    #}
}
