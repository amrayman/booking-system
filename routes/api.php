<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\TripController;
use App\Http\Controllers\StationController;
use App\Http\Controllers\BusController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

#Middleware requires request to have a valid bearer token.
Route::group(['middleware' => ['auth:api']], function ()
{
	Route::post('/book/index', [BookingController::class, 'index']);
	
});

#For Dev Not APIs
Route::get('/user/create', [UserController::class, 'create'])->name("user.create");
Route::post('/trip', [TripController::class, 'create']);
Route::get('/trip/{id}/route', [TripController::class, 'showRoute']);
Route::post('/bus', [BusController::class, 'create']);
Route::post('/station', [StationController::class, 'create']);

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

});
*/
